﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace VatCompWeb.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IConfiguration configuration;

        [BindProperty]
        public decimal Sales { get; set; }
        [BindProperty]
        public decimal Vat { get; set; }
        [BindProperty]
        public decimal Total { get; set; }

        public IndexModel(ILogger<IndexModel> logger, IConfiguration configuration)
        {
            _logger = logger;
            this.configuration = configuration;
            this.Sales = 0;
            this.Vat = 0;
            this.Total = 0;
        }

        public void OnGet()
        {
            _logger.LogInformation($"Found VatCompApi key: {this.configuration.GetValue<string>("VatCompApiKey")}");
        }

        public void OnPost()
        {
            if (this.Vat == 0)
                throw new InvalidOperationException($"{nameof(Vat)} Cannot be 0");
            this.Total = this.Sales * this.Vat;
        }
    }
}